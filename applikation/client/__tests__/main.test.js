import { filterByMaxWinePrice, filterByMaxMenuPrice } from "../src/Main";

import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import axios from "axios";
import AddResti from "../src/Main";

jest.mock("axios");

describe("AddResti component", () => {
  it("should submit the form successfully", async () => {
    const mockedRestaurant = {
      id: 1,
      restaurantName: "Test Restaurant",
      address: {
        street: "Test Street",
        city: "Test City",
        state: "Test State",
        zip: "12345",
        gmaps: "https://maps.google.com",
      },
      menu: {
        name: "Test Menu",
        price: 10.99,
      },
      wine: {
        name: "Test Wine",
        price: 8.99,
      },
      review: {
        stars: 4,
        comment: "Great place!",
        date_of_review: "01.01.2022 12:34:56",
      },
    };

    axios.post.mockResolvedValueOnce();

    const { getByLabelText, getByText } = render(<AddResti />);

    fireEvent.change(getByLabelText("Name:"), {
      target: { value: mockedRestaurant.restaurantName },
    });
    fireEvent.change(getByLabelText("Straße:"), {
      target: { value: mockedRestaurant.address.street },
    });
    fireEvent.change(getByLabelText("Stadt:"), {
      target: { value: mockedRestaurant.address.city },
    });
    fireEvent.change(getByLabelText("Bundesland:"), {
      target: { value: mockedRestaurant.address.state },
    });
    fireEvent.change(getByLabelText("PLZ:"), {
      target: { value: mockedRestaurant.address.zip },
    });
    fireEvent.change(getByLabelText("Google Maps Link:"), {
      target: { value: mockedRestaurant.address.gmaps },
    });
    fireEvent.change(getByLabelText("Speisekarte - Name:"), {
      target: { value: mockedRestaurant.menu.name },
    });
    fireEvent.change(getByLabelText("Speisekarte - Preis:"), {
      target: { value: mockedRestaurant.menu.price },
    });
    fireEvent.change(getByLabelText("Wein - Name:"), {
      target: { value: mockedRestaurant.wine.name },
    });
    fireEvent.change(getByLabelText("Wein - Preis:"), {
      target: { value: mockedRestaurant.wine.price },
    });
    fireEvent.change(getByLabelText("Bewertung - Sterne:"), {
      target: { value: mockedRestaurant.review.stars },
    });
    fireEvent.change(getByLabelText("Bewertung - Kommentar:"), {
      target: { value: mockedRestaurant.review.comment },
    });

    fireEvent.click(getByText("Hinzufügen"));

    await waitFor(() => expect(axios.post).toHaveBeenCalledTimes(1));
  });
});
describe("testing filter function", () => {
  const restaurants = [
    {
      name: "Restaurant A",
      wine: { price: 20 },
      menu: { price: 30 },
      review: { stars: 4.5 },
    },
  ];
  const setFilteredRestaurantsMock = jest.fn();

  test("filterByMaxWinePrice filters and sorts restaurants correctly", () => {
    filterByMaxWinePrice(25, restaurants, setFilteredRestaurantsMock);

    expect(setFilteredRestaurantsMock).toHaveBeenCalledWith([]);
  });

  test("filterByMaxMenuPrice filters and sorts restaurants correctly", () => {
    filterByMaxMenuPrice(35, restaurants, setFilteredRestaurantsMock);

    expect(setFilteredRestaurantsMock).toHaveBeenCalledWith([]);
  });
});
