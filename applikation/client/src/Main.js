import React, { useState, useEffect } from "react";
import axios from "axios";

function AddResti() {
  const [restaurants, setRestaurants] = useState([]);
  const [showAddForm, setShowAddForm] = useState(false);
  const [showEditForm, setShowEditForm] = useState(false);
  const [selectedFilter, setSelectedFilter] = useState("");
  const [filterValue, setFilterValue] = useState("");
  const [filteredRestaurants, setFilteredRestaurants] = useState([]);
  const [editingRestaurant, setEditingRestaurant] = useState(null);
  const [newRestaurant, setNewRestaurant] = useState({
    restaurantName: "",
    address: {
      street: "",
      city: "",
      state: "",
      zip: "",
      gmaps: "",
    },
    menu: {
      name: "",
      price: 0,
    },
    wine: {
      name: "",
      price: 0,
    },
    review: {
      stars: 0,
      comment: "",
      date_of_review: "",
    },
  });

  useEffect(() => {
    loadLatestReviews();
  }, []);

  const baseUrl = "http://localhost:3001/restaurant";

  const loadLatestReviews = async () => {
    try {
      const response = await axios.get("http://localhost:3001/restaurant");

      const sortedRestaurants = response.data.sort((a, b) => {
        const dateA = new Date(a.review.date_of_review);
        const dateB = new Date(b.review.date_of_review);
        return dateB - dateA;
      });

      const reviewsToShow =
        selectedFilter === "all"
          ? sortedRestaurants
          : sortedRestaurants.slice(0, 3);

      setRestaurants(reviewsToShow);
    } catch (error) {
      console.error("Fehler beim Laden der Restaurants:", error.message);
    }
  };
  const loadRestaurants = async () => {
    try {
      const response = await axios.get("http://localhost:3001/restaurant");
      setRestaurants(response.data);
    } catch (error) {
      console.error("Fehler beim Laden der Restaurants:", error.message);
    }
  };

  const handleDelete = async (restaurantId) => {
    console.log("delete");
    try {
      await axios.delete(`http://localhost:3001/restaurant/${restaurantId}`);

      setRestaurants((prevRestaurants) => {
        console.log("Previous Restaurants:", prevRestaurants);
        const updatedRestaurants = prevRestaurants.filter(
          (restaurant) => restaurant.id !== restaurantId
        );
        console.log("Updated Restaurants:", updatedRestaurants);
        return updatedRestaurants;
      });
    } catch (error) {
      console.error("Fehler beim Löschen des Restaurants:", error.message);
    }
  };

  const addForm = () => {
    setShowAddForm(true);
  };
  const addEditForm = (selectedRestaurant) => {
    setEditingRestaurant(selectedRestaurant);
    setShowEditForm(true);
  };

  const Submit = async () => {
    try {
      const existingRestaurant = restaurants.find(
        (restaurant) =>
          restaurant.restaurantName === newRestaurant.restaurantName &&
          restaurant.address.street === newRestaurant.address.street &&
          restaurant.address.city === newRestaurant.address.city &&
          restaurant.address.state === newRestaurant.address.state &&
          restaurant.address.zip === newRestaurant.address.zip
      );

      if (existingRestaurant) {
        alert(
          "Ein Restaurant mit dem gleichen Namen und derselben Adresse existiert bereits."
        );
        return;
      }
      const currentDate = new Date();

      const day = currentDate.getDate();
      const month = currentDate.getMonth() + 1;
      const year = currentDate.getFullYear();
      const hours = currentDate.getHours();
      const minutes = currentDate.getMinutes();
      const seconds = currentDate.getSeconds();

      const formattedDate = `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;

      const restaurantWithTime = {
        ...newRestaurant,
        review: {
          ...newRestaurant.review,
          date_of_review: formattedDate,
        },
      };
      await axios.post(baseUrl, restaurantWithTime);
      await loadRestaurants();

      setNewRestaurant({
        restaurantName: "",
        address: {
          street: "",
          city: "",
          state: "",
          zip: "",
          gmaps: "",
        },
        menu: {
          name: "",
          price: 0,
        },
        wine: {
          name: "",
          price: 0,
        },
        review: {
          stars: 0,
          comment: "",
          date_of_review: "",
        },
      });

      setShowAddForm(false);
    } catch (error) {
      console.error("Fehler beim Hinzufügen des Restaurants:", error.message);
    }
  };
  const submitEdit = async () => {
    try {
      const currentDate = new Date();

      const day = currentDate.getDate();
      const month = currentDate.getMonth() + 1;
      const year = currentDate.getFullYear();
      const hours = currentDate.getHours();
      const minutes = currentDate.getMinutes();
      const seconds = currentDate.getSeconds();

      const formattedDate = `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;
      const updatedRestaurant = {
        ...editingRestaurant,
        review: {
          ...editingRestaurant.review,
          date_of_review: formattedDate,
        },
      };
      await axios.put(`${baseUrl}/${editingRestaurant.id}`, updatedRestaurant);
      loadRestaurants();
      setEditingRestaurant(null);
      setShowEditForm(false);
    } catch (error) {
      console.error("Fehler beim Bearbeiten des Restaurants:", error.message);
    }
  };
  const onCancel = () => {
    setShowEditForm(false);
    setEditingRestaurant(null);
  };
  const onInputChange = (field, value) => {
    if (showAddForm) {
      setNewRestaurant((prevRestaurant) => ({
        ...prevRestaurant,
        [field]: value,
      }));
    } else if (showEditForm && editingRestaurant) {
      setEditingRestaurant((prevRestaurant) => ({
        ...prevRestaurant,
        [field]: value,
      }));
    }
  };
  const applyFilter = () => {
    switch (selectedFilter) {
      case "topRated":
        filterTopRatedRestaurants(filterValue);
        break;
      case "maxWinePrice":
        filterByMaxWinePrice(filterValue);
        break;
      case "maxMenuPrice":
        filterByMaxMenuPrice(parseFloat(filterValue));
        break;
      case "all":
        loadRestaurants();
      default:
        console.log("Ungültiger Filtertyp");
    }
  };

  const filterTopRatedRestaurants = () => {
    const restaurantsWithSameStars = restaurants.filter(
      (restaurant) => restaurant.review.stars === parseInt(filterValue)
    );
    setFilteredRestaurants(restaurantsWithSameStars.slice(0, 5));
  };

  const filterByMaxWinePrice = (maxWinePrice) => {
    const filteredRestaurants = restaurants
      .filter((restaurant) => restaurant.wine.price <= maxWinePrice)
      .sort((a, b) => b.review.stars - a.review.stars);
    setFilteredRestaurants(filteredRestaurants.slice(0, 5));
  };

  const filterByMaxMenuPrice = (maxMenuPrice) => {
    const filteredRestaurants = restaurants
      .filter((restaurant) => restaurant.menu.price <= maxMenuPrice)
      .sort((a, b) => b.review.stars - a.review.stars);
    setFilteredRestaurants(filteredRestaurants.slice(0, 4));
  };

  const resetFilter = () => {
    setFilteredRestaurants([]);
    setSelectedFilter("");
    setFilterValue("");
  };
  const handleFilterChange = (event) => {
    setSelectedFilter(event.target.value);
    if (event.target.value === "all") {
      loadRestaurants();
    }
  };

  const handleFilterValueChange = (event) => {
    setFilterValue(event.target.value);
  };
  const restaurantsToDisplay =
    filteredRestaurants.length > 0 ? filteredRestaurants : restaurants;
  return (
    <div>
      <h1>Restaurant Liste</h1>
      <div className="addIcon" style={{ marginBottom: "10px" }}>
        <button onClick={addForm}>hinzufügen</button>
      </div>
      <div>
        <label>Filter:</label>
        <select value={selectedFilter} onChange={handleFilterChange}>
          <option value="">Auswählen</option>
          <option value="topRated">Top bewertete Restaurants</option>
          <option value="maxWinePrice">Maximaler Weinpreis</option>
          <option value="maxMenuPrice">Maximaler Menupreis</option>
          <option value="all">Alle Restaurants anzeigen</option>
        </select>

        {selectedFilter && selectedFilter !== "all" && (
          <div>
            <label>Filterwert:</label>
            <input
              type="number"
              value={filterValue}
              onChange={handleFilterValueChange}
              id="filterwert"
            />
            <button onClick={applyFilter}>Filter anwenden</button>
            <button onClick={resetFilter}>Filter aufheben</button>
          </div>
        )}
      </div>
      <div>
        {showEditForm && editingRestaurant && (
          <form className="add-form" onSubmit={submitEdit}>
            <div className="form-group">
              <label>Name:</label>
              <input
                type="text"
                value={editingRestaurant.restaurantName}
                onChange={(e) =>
                  onInputChange("restaurantName", e.target.value)
                }
              />
            </div>

            <div className="form-group">
              <label>Adresse:</label>
              <div>
                <label>Straße:</label>
                <input
                  type="text"
                  value={editingRestaurant.address.street}
                  onChange={(e) =>
                    onInputChange("address", {
                      ...editingRestaurant.address,
                      street: e.target.value,
                    })
                  }
                />
              </div>
              <div>
                <label>Stadt:</label>
                <input
                  type="text"
                  value={editingRestaurant.address.city}
                  onChange={(e) =>
                    onInputChange("address", {
                      ...editingRestaurant.address,
                      city: e.target.value,
                    })
                  }
                />
              </div>
              <div>
                <label>Bundesland:</label>
                <input
                  type="text"
                  value={editingRestaurant.address.state}
                  onChange={(e) =>
                    onInputChange("address", {
                      ...editingRestaurant.address,
                      state: e.target.value,
                    })
                  }
                />
              </div>
              <div>
                <label>PLZ:</label>
                <input
                  type="text"
                  value={editingRestaurant.address.zip}
                  onChange={(e) =>
                    onInputChange("address", {
                      ...editingRestaurant.address,
                      zip: e.target.value,
                    })
                  }
                />
              </div>
              <div>
                <label>Google Maps Link:</label>
                <input
                  type="text"
                  value={editingRestaurant.address.gmaps}
                  onChange={(e) =>
                    onInputChange("address", {
                      ...editingRestaurant.address,
                      gmaps: e.target.value,
                    })
                  }
                />
              </div>
            </div>

            <div className="form-group">
              <label>Speisekarte - Name:</label>
              <input
                type="text"
                value={editingRestaurant.menu.name}
                onChange={(e) =>
                  onInputChange("menu", {
                    ...editingRestaurant.menu,
                    name: e.target.value,
                  })
                }
              />
            </div>

            <div className="form-group">
              <label>Speisekarte - Preis:</label>
              <input
                type="number"
                value={editingRestaurant.menu.price}
                onChange={(e) =>
                  onInputChange("menu", {
                    ...editingRestaurant.menu,
                    price: parseFloat(e.target.value),
                  })
                }
              />
            </div>

            <div className="form-group">
              <label>Wein - Name:</label>
              <input
                type="text"
                value={editingRestaurant.wine.name}
                onChange={(e) =>
                  onInputChange("wine", {
                    ...editingRestaurant.wine,
                    name: e.target.value,
                  })
                }
              />
            </div>

            <div className="form-group">
              <label>Wein - Preis:</label>
              <input
                type="number"
                value={editingRestaurant.wine.price}
                onChange={(e) =>
                  onInputChange("wine", {
                    ...editingRestaurant.wine,
                    price: parseFloat(e.target.value),
                  })
                }
              />
            </div>

            <div className="form-group">
              <label>Bewertung - Sterne:</label>
              <input
                type="number"
                value={editingRestaurant.review.stars}
                onChange={(e) =>
                  onInputChange("review", {
                    ...editingRestaurant.review,
                    stars: parseInt(e.target.value),
                  })
                }
              />
            </div>

            <div className="form-group">
              <label>Bewertung - Kommentar:</label>
              <input
                type="text"
                value={editingRestaurant.review.comment}
                onChange={(e) =>
                  onInputChange("review", {
                    ...editingRestaurant.review,
                    comment: e.target.value,
                  })
                }
              />
            </div>

            <div className="form-group">
              <button type="submit">Aktualisieren</button>
              <button type="button" onClick={onCancel}>
                Abbrechen
              </button>
            </div>
          </form>
        )}
      </div>
      <div>
        {showAddForm && (
          <form className="add-form" onSubmit={Submit}>
            <div className="form-group">
              <label>Name:</label>
              <input
                type="text"
                value={newRestaurant.restaurantName}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    restaurantName: e.target.value,
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Straße:</label>
              <input
                type="text"
                value={newRestaurant.address.street}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    address: {
                      ...newRestaurant.address,
                      street: e.target.value,
                    },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Stadt:</label>
              <input
                type="text"
                value={newRestaurant.address.city}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    address: { ...newRestaurant.address, city: e.target.value },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Bundesland:</label>
              <input
                type="text"
                value={newRestaurant.address.state}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    address: {
                      ...newRestaurant.address,
                      state: e.target.value,
                    },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>PLZ:</label>
              <input
                type="text"
                value={newRestaurant.address.zip}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    address: { ...newRestaurant.address, zip: e.target.value },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Google Maps Link:</label>
              <input
                type="text"
                value={newRestaurant.address.gmaps}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    address: {
                      ...newRestaurant.address,
                      gmaps: e.target.value,
                    },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Speisekarte - Name:</label>
              <input
                type="text"
                value={newRestaurant.menu.name}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    menu: { ...newRestaurant.menu, name: e.target.value },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Speisekarte - Preis:</label>
              <input
                type="number"
                value={newRestaurant.menu.price}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    menu: {
                      ...newRestaurant.menu,
                      price: parseFloat(e.target.value),
                    },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Wein - Name:</label>
              <input
                type="text"
                value={newRestaurant.wine.name}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    wine: { ...newRestaurant.wine, name: e.target.value },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Wein - Preis:</label>
              <input
                type="number"
                value={newRestaurant.wine.price}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    wine: {
                      ...newRestaurant.wine,
                      price: parseFloat(e.target.value),
                    },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Bewertung - Sterne:</label>
              <input
                type="number"
                value={newRestaurant.review.stars}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    review: {
                      ...newRestaurant.review,
                      stars: parseInt(e.target.value),
                    },
                  })
                }
              />
            </div>
            <div className="form-group">
              <label>Bewertung - Kommentar:</label>
              <input
                type="text"
                value={newRestaurant.review.comment}
                onChange={(e) =>
                  setNewRestaurant({
                    ...newRestaurant,
                    review: {
                      ...newRestaurant.review,
                      comment: e.target.value,
                    },
                  })
                }
              />
            </div>

            <div className="form-group">
              <button type="submit">Hinzufügen</button>
            </div>
          </form>
        )}
      </div>
      {restaurantsToDisplay.map((restaurant) => (
        <div className="main" key={restaurant.id}>
          <div className="restaurant-card">
            <h2>{restaurant.restaurantName}</h2>
            <p>
              {restaurant.address.street}, {restaurant.address.city}
            </p>
            <p>Rating: {restaurant.review.stars}</p>
            <p>Datum der Bewertung: {restaurant.review.date_of_review}</p>
            <p>
              Wein: {restaurant.wine.name}, {restaurant.wine.price} CHF
            </p>
            <p>
              Menu: {restaurant.menu.name}, {restaurant.menu.price} CHF
            </p>
            <p>Kommentar: {restaurant.review.comment}</p>
            <p>
              <a href={restaurant.address.gmaps} target="_blank">
                Google Maps
              </a>
            </p>

            <>
              <button onClick={() => addEditForm(restaurant)}>
                bearbeiten
              </button>
              <button onClick={() => handleDelete(restaurant.id)}>
                Löschen
              </button>
            </>
          </div>
        </div>
      ))}
    </div>
  );
}

export default AddResti;
