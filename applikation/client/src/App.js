import "./App.css";
import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AddResti from "./Main";

function App() {
  return (
    <div>
      <div>
        <Router>
          <div className="App">
            <Routes>
              <Route path="/" element={<AddResti />} />
            </Routes>
          </div>
        </Router>
      </div>
    </div>
  );
}

export default App;
